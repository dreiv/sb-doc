import MyButton, { buttonVariants, buttonSizes } from './index.vue';
import OverflowCenter from '../../helpers/decorators/OverflowCenter';

export default {
  title: 'Example/Button',
  component: MyButton,
  argTypes: {
    size: {
      control: {
        type: 'select',
        options: buttonSizes
      }
    },
    variant: {
      control: {
        type: 'select',
        options: buttonVariants
      }
    },
  },
  args: {
    label: 'Button'
  }
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { MyButton },
  template: '<my-button v-bind="$props"/>'
});

export const Default = Template.bind({});

export const Outlined = Template.bind({});
Outlined.args = {
  variant: 'outlined'
};

export const Text = Template.bind({});
Text.args = {
  variant: 'text'
};

export const Small = Template.bind({});
Small.args = {
  size: 'small'
};

export const Large = Template.bind({});
Large.args = {
  size: 'large'
};

export const Wrapped = Template.bind({});
Wrapped.decorators = [() => ({
  components: { OverflowCenter },
  template: `
    <overflow-center>
      <story />
    </overflow-center>`
})];
