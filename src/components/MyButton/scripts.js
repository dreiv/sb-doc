export const buttonVariants = ['contained', 'outlined', 'text'];
export const buttonSizes = ['small', 'medium', 'large'];

export default {
  props: {
    label: {
      type: String,
      required: true,
    },
    variant: {
      type: String,
      default: 'contained',
      validator: (value) => {
        return buttonVariants.includes(value)
      }
    },
    size: {
      type: String,
      default: 'medium',
      validator: (value) => {
        return buttonSizes.includes(value)
      }
    }
  },

  computed: {
    classes() {
      return {
        [`btn-${this.variant}`]: Boolean(this.variant),
        [`btn-${this.size}`]: Boolean(this.size)
      };
    }
  }
}
